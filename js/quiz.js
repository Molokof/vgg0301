(function() {
  const myQuestions = [
    {
      question: "What does the acronym VGG represent?",
      answers: {
        a: "Value Grading Group",
        b: "Ventura Gladys Gens",
        c: "Venture Garden Group"
      },
      correctAnswer: "c"
    },
    {
      question: "Silicon Valley is to California. ___ is to VGG",
      answers: {
        a: "IdeaHub",
        b: "Bev Valley",
        c: "Vibranium Valley"
      },
      correctAnswer: "c"
    },
    {
      question: "Which of the following are names of rooms in VGG",
      answers: {
        a: "Shepeteri, Anifowoshe, One chance",
        b: "Kuti, 2050, One Chance",
        c: "Isheri, New York, Hub"
      },
      correctAnswer: "b"
    },
    {
      question: "Who's the present CTO?",
      answers: {
        a: "Mogwai Justin",
        b: "Demola Idowu",
        c: "Prosper Otemuyiwa"
      },
      correctAnswer: "b"
    },
    {
      question: "VGG has a mutual theme that runs across which of these movies",
      answers: {
        a: "Superman",
        b: "The Avengers",
        c: "Black Panther",
      },
      correctAnswer: "b"
    }
  ];

  function buildQuiz() {
    // we'll need a place to store the HTML output
    const output = [];

    // for each question...
    myQuestions.forEach((currentQuestion, questionNumber) => {
      // we'll want to store the list of answer choices
      const answers = [];

      // and for each available answer...
      for (letter in currentQuestion.answers) {
        // ...add an HTML radio button
        answers.push(
          `<label>
             <input type="radio" name="question${questionNumber}" value="${letter}">
              ${letter} :
              ${currentQuestion.answers[letter]}
           </label>`
        );
      }

      // add this question and its answers to the output
      output.push(
        `<div class="w3-margin-bottom slide">
           <div class="w3-container w3-center">
           <><>
           <div class="w3-xlarge w3-padding w3-margin-bottom w3-text-white"> ${currentQuestion.question} </div>
           <div class="w3-text-white answers"> ${answers.join("")} </div>
           </div>
         </div>`
      );
    });

    // finally combine our output list into one string of HTML and put it on the page
    quizContainer.innerHTML = output.join("");
  }

  function showResults() {
    // gather answer containers from our quiz
    const answerContainers = quizContainer.querySelectorAll(".answers");

    // keep track of user's answers
    let numCorrect = 0;

    // for each question...
    myQuestions.forEach((currentQuestion, questionNumber) => {
      // find selected answer
      const answerContainer = answerContainers[questionNumber];
      const selector = `input[name=question${questionNumber}]:checked`;
      const userAnswer = (answerContainer.querySelector(selector) || {}).value;

      // if answer is correct
      if (userAnswer === currentQuestion.correctAnswer) {
        // add to the number of correct answers
        numCorrect++;

        // color the answers green
        answerContainers[questionNumber].style.color = "w3-green";
        previousButton.style.display = 'none';
        submitButton.style.display = 'disable';
      } else {
        // if answer is wrong or blank
        // color the answers red
        answerContainers[questionNumber].style.color = "w3-red";
      }
    });

  let hidequizContainer = document.getElementById("quiz").style.display = 'none';
  // let hideresultsContainer = document.getElementById("results").style.display = 'none';
  let hidesubmitButton = document.getElementById("submit").style.display = 'none';
  let hidepreviousButton = document.getElementById("previous").style.display = 'none';
  let hidenextButton = document.getElementById("next").style.display = 'none';
  let hideRealQuizContainer = document.querySelector(".quiz-container").style.display = "none";
  
  // let slides = document.querySelectorAll(".slide");

    // show number of correct answers out of total
    resultsContainer.innerHTML = `<p class="w3-xlarge w3-padding-48 w3-margin w3-center w3-card-4 w3-dark-grey" style="text-shadow:1px 1px 0 #444">You got ${numCorrect} answers right out of ${myQuestions.length}</p>`;
    resultsContainer.innerHTML += `<a href ="index.html" id="" class="w3-btn w3-display-middle w3-blue w3-hover-text-white w3-large"><i class="fa fa-home"></i>Go To Home</a>`


  }

  function showSlide(n) {
    slides[currentSlide].classList.remove("active-slide");
    slides[n].classList.add("active-slide");
    currentSlide = n;
    
    if (currentSlide === 0) {
      previousButton.style.display = "none";
    } else {
      previousButton.style.display = "inline-block";
    }
    
    if (currentSlide === slides.length - 1) {
      nextButton.style.display = "none";
      submitButton.style.display = "inline-block";
    } else {
      nextButton.style.display = "inline-block";
      submitButton.style.display = "none";
    }
  }

  function showNextSlide() {
    showSlide(currentSlide + 1);
  }

  function showPreviousSlide() {
    showSlide(currentSlide - 1);
  }

  function goHome(){
    return `<a href="index.html"></a>`
  }

  const quizContainer = document.getElementById("quiz");
  const resultsContainer = document.getElementById("results");
  const submitButton = document.getElementById("submit");
  const realQuizContainer = document.querySelector(".quiz-container")

  // display quiz right away
  buildQuiz();

  const previousButton = document.getElementById("previous");
  const nextButton = document.getElementById("next");
  const slides = document.querySelectorAll(".slide");
  const home = document.querySelectorAll(".home");
  let currentSlide = 0;

  showSlide(0);

  // on submit, show results
  submitButton.addEventListener("click", showResults);
  previousButton.addEventListener("click", showPreviousSlide);
  nextButton.addEventListener("click", showNextSlide);
  home.addEventListener("click", goHome);
})();

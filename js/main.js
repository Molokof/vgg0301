// Variables
let jobList = document.getElementById("jobList");

// Read Jobs From Local Storage => return Array of jobs  or false
function getJobsFromLocalStorage() {
  let dataFromLocalStorage = localStorage.getItem("jobs");
  if (dataFromLocalStorage && Array.isArray(JSON.parse(dataFromLocalStorage))) {
    //check true or false
    return JSON.parse(dataFromLocalStorage);
  } else {
    return false;
  }
}

let jobStatus = "Open";

function viewJob() {
  let jobs = JSON.parse(localStorage.getItem("jobs"));
  let viewJobTitle = document.getElementById("viewJobTitle");
  let viewJobs = document.getElementById("viewJobs");

  viewJobTitle.innerHTML = "";
  viewJobs.innerHTML = "";

  for (let i = 0; i < jobs.length; i++) {
    console.log(jobs[i]);
    let company = jobs[i].company;
    let email = jobs[i].email;
    let jobTitle = jobs[i].jobTitle;
    let location = jobs[i].location;
    let expiry = jobs[i].expiry;
    let desc = jobs[i].description;
    let status = jobs[i].status;

    viewJobTitle = `<div class="w3-panel w3-card">
                <h1 class="w3-text-white font-weight-bold">Job Title: ${jobTitle}</h1>
            </div>`;

    viewJob = `<div>
                  <span class="w3-text-teal">Company: ${company}</span>
                  <p>Email: ${email}</p>
                  <code>Entry closes: ${expiry}</code>
                  <section>
                    <textarea>
                      <h3>Description</h3>
                      <p>${desc}</p>
                    </textarea>
                  </section>
                </div>`;
  }
}

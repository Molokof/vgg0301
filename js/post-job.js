let form = document.getElementById("createJobForm");
let outputDiv = document.getElementById('jobList');

// Event Listeners
form.addEventListener("submit", saveJobLocStorage);
// outputDiv.addEventListener('click', delJobsDOM);


function saveJobLocStorage(e) {
  e.preventDefault();
  
  let jobId = new Date().getTime();
  let company = document.getElementById("company-name").value;
  let mailing = document.getElementById("email").value;
  let job = document.getElementById("job-title").value;
  let geolocation = document.getElementById("location").value;
  let exp = document.getElementById("date").value;
  let description = document.getElementById("job-desc").value;

  let jobObj = {
    jobId: jobId,
    companyname: company,
    mail: mailing,
    occupation: job,
    location: geolocation,
    expiry: exp,
    desc: description,
    status: jobStatus
  };

  // console.log(jobObj);

  // Get jobs from Local Storage
  let jobsFromLocalStorage = getJobsFromLocalStorage();

  if (!jobsFromLocalStorage) {
    let newJobsArray = [];
    newJobsArray.push(jobObj);
    localStorage.setItem("jobs", JSON.stringify(newJobsArray));
  } else {
    jobsFromLocalStorage.push(jobObj);
    localStorage.setItem("jobs", JSON.stringify(jobsFromLocalStorage));
  }

  fetchJobsLocStorage();

  form.reset();
}

// Functions
function fetchJobsLocStorage() {
  let jobs = getJobsFromLocalStorage();
  if (jobs && jobs.length > 0) {
    let jobComponent = "";
    for (let i = 0; i < jobs.length; i++) {
      // console.log(jobs[i]);
      let currentJob = jobs[i];

      jobComponent += `
                    <p>
                        <span class="w3-btn w3-blue w3-animate-opacity w3-margin-top">${currentJob.status}</span>
                    </p>
                    <br>
                    <h4><i class="fa fa-building"></i> Company: ${currentJob.companyname}</h4>
                    <p>Email: ${currentJob.mail}</p>
                    <p class="w3-xlarge">Job Title: ${currentJob.occupation}</p>
                    <p><i class="fa fa-map-pin"></i> Location: ${currentJob.location}</p>
                    <p>Entry closes: ${currentJob.expiry}</p>
                    <p>Job Description: ${currentJob.desc.length < 50 ? `${currentJob.desc}`:`${currentJob.desc.substring(0, 40)}...`}</p>
                    <button type="button" href="#" id="${currentJob.jobId}" class="w3-btn w3-orange w3-margin-top w3-hover-blue-gray"  onclick="changeJobStatus(this)">Close Entry</button>
                    <a href="#" id="${currentJob.jobId}" class="w3-btn w3-red delete-a w3-margin-top" onclick="deleteJob(this)">Delete Job</a>
                    
                    <hr class="w3-border">`;
                    // <a href="#" class="w3-btn w3-amber w3-right w3-margin-top" id=${currentJob.jobId} onclick="viewMore()">Read more>></a>
    }
    jobList.innerHTML = jobComponent;
  } else {
    jobList.innerHTML = `
                    <p>
                        <span class="w3-card-2 w3-margin-top">No jobs found</span>
                    </p>
                    <br>
                    <hr class="w3-border">`;
  }
}


function deleteJob(target) {
  console.log(target.id);
  let jobs = getJobsFromLocalStorage();
  let filteredJobs = jobs.filter(function(job){
    return parseInt(job.jobId) !== parseInt(target.id)
  });
  localStorage.setItem("jobs", JSON.stringify(filteredJobs));
  fetchJobsLocStorage();
}

function changeJobStatus(target) {
  console.log(target.id);
  let jobs = getJobsFromLocalStorage();
  jobs.map(function(job){
    return job.status = 'Closed';
  });
  document.querySelector('button').style.color = 'w3-grey'
  localStorage.setItem("jobs", JSON.stringify(jobs));
  
  fetchJobsLocStorage();
  console.log(target);
  
  // document.getElementById(`${target.id}`).textContent = 'Open Entry';
}

function viewMore(){
  return
  `
  <button onclick="document.getElementById(${currentJob.jobId}).style.display='block'" class="w3-button w3-black">Open Animated Modal</button>

  <div id=(${currentJob.jobId}) class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-teal"> 
        <span onclick="document.getElementById('id01').style.display='none'" 
        class="w3-button w3-display-topright">&times;</span>
        <h2>Modal Header</h2>
      </header>
      <div class="w3-container">
        <p>Some text..</p>
        <p>Some text..</p>
      </div>
      <footer class="w3-container w3-teal">
        <p>Modal Footer</p>
      </footer>
    </div>
  </div>
</div>
  `
}

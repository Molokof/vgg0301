const search = document.getElementById("searchInput");
let noJob = document.querySelector("#no-jobs");

search.addEventListener("click", mainSearch);

function readFromLocalStorage() {
  let dataStore = localStorage.getItem("jobs");
  if (dataStore) {
    // check if data exists
    return JSON.parse(dataStore);
  } else {
    return false;
  }
}

function mainSearch(e) {
  e.preventDefault();

  const jobInput = document.querySelector("#jobInput").value.trim();
  const jobLocation = document.querySelector("#locationInput").value.trim();
  const filteredJobs = [];

  if ((jobLocation || jobLocation != "") && (jobInput || jobInput != "")) {
    const jobs = readFromLocalStorage();
    if (jobs && jobs.length > 0) {
      for (let i = 0; i < jobs.length; i++) {
        let currentJob = jobs[i];
        console.log("*********Iteration" + (i + 1) + "*********");
        console.log(
          "Title from localstorage: " + currentJob.occupation.toLowerCase()
        );
        console.log("Title from form: " + jobInput);
        console.log("\n");
        console.log(
          "location  from localstorage: " + currentJob.location.toLowerCase()
        );
        console.log("Location from form: " + locationInput);
        console.log("*********End iteration*********");
        console.log("\n\n");
        if (
          currentJob.location
            .toLowerCase()
            .includes(jobLocation.toLowerCase()) &&
          currentJob.occupation.toLowerCase().includes(jobInput.toLowerCase())
        ) {
          filteredJobs.push(currentJob);
          console.log(filteredJobs);
        } else {
          // continue;
          noJob.innerHTML = `<p class="w3-container w3-card-2 w3-xxlarge w3-center w3-teal">${filteredJobs.length} jobs found</p>`;
          // document.querySelector("#jobsList").style.display = "none";
          // document.querySelector("#zero-jobs").style.display = "none";
        }
      }
      console.log(filteredJobs);
    } else {
      console.log("There are no jobs available");
    }
  } else {
    console.log("invalid input");
  }
}


// Variables
const ul = document.getElementById("blogs");
const div = document.getElementById("comments");
const post = document.getElementById("post");
const view = document.getElementById("viewpost");
const page = document.getElementById("pages");

const base = "https://jsonplaceholder.typicode.com/";
const commentsUrl = "https://jsonplaceholder.typicode.com/comments";
const postsUrl = "https://jsonplaceholder.typicode.com/posts?";
// const postOnComments =  `https://jsonplaceholder.typicode.com/posts/${id}/comments?postId=${id}`
// const postUrl =` http://jsonplaceholder.typicode.com/posts`;


function getUrlParams(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
  var results = regex.exec(location.search);
  return results === null
    ? ""
    : decodeURIComponent(results[1].replace(/\+/g, " "));
}

let post_id = getUrlParams("postId");

function getComments(id = post_id) {
  fetch(`${commentsUrl}?postId=${id}`)
    .then(res => res.json())
    .then(data => {
      // console.log(data);
      // let posts = data.data
      data.forEach(element => {
        // console.log(data.length)
        // console.log(element.length)
        div.innerHTML += `
          <div class="w3-panel w3-card-2 w3-padding-16 w3-white w3-large">
            <p><i class="fa fa-male"></i> <b>Name: </b>&nbsp;${element.name}</p>
            <p><i class="fa fa-envelope"></i><b> Email: </b>&nbsp; ${element.email}</p>
            <section><i class="fa fa-clipboard"></i><b> Comments: &nbsp;</b>${element.body}</section>

            
          </div>
        `;
      });
    })
    .catch(error => {
      console.warn("Oops...something went wrong", error);
    });
}

function getBlog(id = post_id) {
  fetch(`${base}posts/${id}`)
    .then(res => res.json())
    .then(data => {
      // console.log(data);
      post.innerHTML = `
        <div class="w3-row">
            <div class="w3-col m4 w3-panel w3-card-2">
            <img src="./assets/vgg.jpg" alt="Image" class="w3-left w3-margin-right" style="width:100%">
            <h3><b>Title: </b>${data.title}</h3>
            </div>
          </div>
        `;
    })
    .catch(error => {
      console.warn("Oops...something went wrong", error);
    });
}

function viewPost(id = post_id) {
  fetch(`${base}posts/${id}`)
    .then(res => res.json())
    .then(data => {
      // console.log(data.length);
      view.innerHTML = `
      <div class="w3-container">
        <h4 class="w3-center"><b>${data.title}</b></h4>
        <div class="w3-center w3-padding w3-margin">
          <img src="./assets/vgg.jpg" alt="Image" class="" style="width:100%, height:10vh">
          <p class="w3-padding w3-margin w3-large">${data.body}</p>
        </div>
        
      </div>
      
    `;
    })
    .catch(error => {
      console.warn("Oops...something went wrong", error);
    });
}

postLimit();

function postLimit (start=0, numberOfPost = 10){
  fetch(`${base}posts?_start=${start}&_limit=${numberOfPost}`)
    .then(res=>res.json())
    .then(data => {
      console.log(data)
      getPosts(data)
    })
    .catch((err) => {
      document.querySelector('.blogError').innerHTML = 'Fail to fetch:Check Your Internet Connection';
  });
}

page === null ? '' : page.addEventListener('click', pagination)

function pagination(e) {
  // alert('yes')
  if(e.target.innerHTML == 1){
    postLimit(limit=20)
  }
  if(e.target.innerHTML == 2){
    postLimit(limit = 30)
  }
  if(e.target.innerHTML == 3){
    postLimit(limit = 40)
  }
  if(e.target.innerHTML == 4){
    postLimit(limit = 50)
  }

  setTimeout(()=> {
    window.scroll({
      top: 50,
      left: 0,
      behavior: 'smooth'
    }, 1000)
  })
}

// Functions
function getPosts(data) {
  ul.innerHTML = "";
  data.forEach(element => {
    //console.log(element.id)
        ul.innerHTML +=`
        <div class="w3-card-4 w3-margin w3-white">
          <div class="w3-row">
            <div class="w3-col w3-half">
            <img src="https://source.unsplash.com/collection/190727/200x150" alt="Nature" style="width:100%">
            </div>

            <div class="w3-half">
            <div class="w3-container">
            <h4><b>${element.title.length < 10 ? `${element.title}`:`${element.title.substring(0, 20)}...`}</b></h4>
          </div>

          <div class="w3-container">
            <p>${element.body.length < 100 ? `${element.body}`:`${element.body.substring(0, 40)}...`}</p>
            <div class="w3-row">
              <div class="w3-col m8 s12">
                <p><a href="../view-post.html?postId=${element.id}" class="w3-button w3-padding w3-white w3-border w3-margin-bottom"><b>READ MORE »</b></a></p>

                <div class="w3-row w3-padding-large">

                  <div class="w3-col l7 w3-left">
                    <img src="../assets/post.png" style="width:210%">
                    <div class="w3-padding-large w3-margin"></div>
                    <p><a href="../comments.html?postId=${element.id}" class="w3-hover-opacity" style="text-decoration: none"><b>Comments</b><span class="w3-tag">5</span></a></p>
                  </div>
                  <div class="w3-col s4 w3-right">

                  </div>
                </div>
              </div>

            </div>
          </div>
            </div>
          </div>

        </div>`

      });
    
    

}


getBlog();
getComments();
viewPost();
pagination()
